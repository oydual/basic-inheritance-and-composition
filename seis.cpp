#include <iostream>

using namespace std;
enum nota { middleC, Csharp, Cflat };

class Instrumento {
public:
void tocar(nota a) const {
        switch (a)
        {
        case middleC:
                cout << "Tocando middleC" << endl;
                break;
        case Csharp:
                cout<< "Tocando Csharp" << endl;
                break;
        case Cflat:
                cout << "Tocando Cflat" << endl;
                break;
        default:
                break;
        }
}
};
class Viento : public Instrumento {};
class Cuerda : public Instrumento {};
class Percusion : public Instrumento {};

void afinar(Instrumento& i) {
        i.tocar(middleC);
}

int main() {
        Viento flauta;
        Instrumento *cualquiera;
        Instrumento armario[3];

        cualquiera = new Cuerda();
        armario[0] = Viento();
        armario[1] = Cuerda();
        armario[2] = Percusion();
        afinar(flauta);
        armario[1].tocar(Csharp);

}
