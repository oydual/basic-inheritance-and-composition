#include <iostream>
#include <string>
using namespace std;

class Padre {
public:
int func() const {
        cout << "Base::func()\n";
        return 1;
}
int func(string s) const {
        cout << "Base::func(string)" << s << "\n";
        return 1;
}
void otherFunc() {
        cout << "Base::otherFunc()\n";
}
};

class Hija1 : public Padre {
public:
void otherFunc() const {
        cout << "Hija1::otherFunc()\n";
}
};

class Hija2 : public Padre {
public:

int func() const {
        cout << "Hija2::func()\n";
        return 2;
}
};

class Hija3 : public Padre {
public:
void func() const {
        cout << "Hija3::func()\n";
}
};

class Hija4 : public Padre {
public:
int func(int a) const {
        cout << "Hija4::func()\n";
        return 4;
}
};

int main() {
        string s("hello");
        Hija1 d1;
        int x = d1.func();
        d1.func(s);
        Hija2 d2;
        x = d2.func();
        Hija3 d3;
        //x = d3.func();
        Hija4 d4;
        //x = d4.func();
        x = d4.func(1);
}
