#include <iostream>
using namespace std;

class Tablero {
public:
Tablero() {
        cout << "Tablero()\n";
}
Tablero(const Tablero&) {
        cout << "Tablero(const Tablero&)\n";
}
Tablero& operator=(const Tablero&) {
        cout << "Tablero::operator=()\n";
        return *this;
}
~Tablero() {
        cout << "~Tablero()\n";
}
};

class Juego {
Tablero tb;
public:
Juego() {
        cout << "Juego()\n";
}
Juego(const Juego& j) : tb(j.tb) {
        cout << "Juego(const Juego&)\n";
}
Juego(int a) {
        cout << "Juego(int)\n";
}
Juego& operator=(const Juego& j) {
        tb = j.tb;
        cout << "Juego::operator=()\n";
        return *this;
}
class Other {
private:
int a;
public:
Other(){
}
};
operator Other() const {
        cout << "Juego::operator Other()\n";
        return Other();
}
~Juego() {
        cout << "~Juego()\n";
}
};

class Ajedrez : public Juego {};

void f(Juego::Other) {
}

class Damas : public Juego {
public:
Damas() {
        cout << "Damas()\n";
}
Damas(const Damas& c) : Juego(c) {
        cout << "Damas(const Damas& c)\n";
}
Damas& operator=(const Damas& c) {
        Juego::operator=(c);
        cout << "Damas::operator=()\n";
        return *this;
}
};

int main() {
        Ajedrez d1;
        Ajedrez d2(d1);
        //Ajedrez d3(1);
        d1 = d2;
        f(d1);
        Juego::Other go;
        //d1 = go;
        Damas c1, c2(c1);
        c1 = c2;
}
