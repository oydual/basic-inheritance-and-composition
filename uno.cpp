#include <iostream>

class A {
int _a;
public:
A(int a) : _a(a) {
        cout << "Contruyendo A" << endl;
}
~A() {
        cout << "Destruyendo A" << endl;
}
void func() const {
        cout << "Funcion de A" << endl;
}
};

class B {
int _b;
public:
B(int b) : _b(b) {
        cout << "Construyendo B" << endl;
}
~B() {
        cout << "Destruyendo B" << endl;
}
void func() const {
        cout << "Funcion de B" << endl;
}
};

class C : public B {
A _a;
public:
C(int c) : B(c), _a(c) {
        cout << "Construyendo C" << endl;
}
~C() {
        cout << "Destruyendo C" endl;
}
void func() const {
        _a.func();
        B::func();
}
};

int main() {
        C c(47);
        C.func();
} ///:~
