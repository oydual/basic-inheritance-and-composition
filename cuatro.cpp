class Animal {
public:
char comer() const {
        return 'a';
}
int hablar() const {
        return 2;
}
float dormir() const {
        return 3.0;
}
float dormir(int) const {
        return 4.0;
}
};

class Pez : Animal {
public:
using Animal::comer;
using Animal::dormir;
};

int main() {
        Pez bob;
        bob.comer();
        bob.dormir();
        bob.dormir(1);
        //bob.hablar();
}
