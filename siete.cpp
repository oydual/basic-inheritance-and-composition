#include <iostream>
using namespace std;

class Padre {
int _i;
public:
Padre(int i) : _i(i) {
        cout << "Padre(int i)\n";
}
Padre(const Padre& b) : _i(b._i) {
        cout << "Padre(const Padre&)\n";
}
Padre() : _i(0) {
        cout << "Padre()\n";
}
friend ostream& operator<<(ostream& os, const Padre& b) {
        return os << "Padre: " << b._i << endl;
}
};

class Miembro {
int _m;
public:
Miembro(int m) : _m(m) {
        cout << "Miembro(int m)\n";
}
Miembro(const Miembro& m) : _m(m._m) {
        cout << "Miembro(const Miembro&)\n";
}
friend ostream& operator<<(ostream& os, const Miembro& m) {
        return os << "Miembro: " << m._m << endl;
}
};

class Hija : public Padre {
int _h;
Miembro _m;
public:
Hija(int h) : Padre(h), _h(h), _m(h) {
        cout << "Hija(int h)\n";
}
friend ostream& operator<<(ostream& os, const Hija& c){
        return os << (Padre&)c << c._m << "Hija: " << c._h << endl;
}
};

int main() {
        Hija c(2);
        cout << "-----------------------" << endl;
        Hija c2 = c;
        cout << "ostream:\n" << c2;
} ///:~
