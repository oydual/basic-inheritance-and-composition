#ifndef CPRODUCTOS
#define CPRODUCTOS
#include <string>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <iomanip>


class CProducto{

	unsigned int codigo;
	unsigned int cantidad; 
	float precio; 
	std::string descripcion; 

   public:
   
      CProducto();
      CProducto(const CProducto&);
      ~CProducto(){}
      
      /**
      @brife get
      */
      inline unsigned int ver_codigo() const { return (this->codigo); }
	
	  inline unsigned int ver_cantidad() const { return(this->cantidad); }
	  
	  inline float ver_precio() const { return(this->precio); }
	  
	  inline std::string ver_descripcion() const { return(this->descripcion); }
	  
	  /**
	  @brife set
	  */
	  
	  void asignar_codigo(unsigned int);
	  void modificar_cantidad(unsigned int);  
	  void asignar(unsigned int , unsigned int , float , std::string); 
	  
	  /**
	  @brife Sobrecarga de operadores
	  */
	  
	   CProducto operator =(const CProducto &);
      //operator = Copiar un producto en el otro

      int operator ==(const CProducto &);
      //operator == Compara si dos productos son iguales return (1) si no, return(0)
      // Buscaré un producto por el código exacto

}; 

 	  std::ostream& operator <<(std::ostream&,const CProducto &);
      std::istream& operator >>(std::istream&, CProducto &);
     
#endif
