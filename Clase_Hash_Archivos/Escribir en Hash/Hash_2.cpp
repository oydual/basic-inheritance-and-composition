#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <iomanip>

int main()
{
	std::fstream Archivo_Hash;

	std::string Clave;
	std::string Aux;
	std::string Aux2;
	std::string Aux3;

	long int Pos;

	Archivo_Hash.open("Hash.txt");

	if(!Archivo_Hash.good())
		return 2;

	std::cout << "Introduzca la Clave: " << std::endl;
	std::getline(std::cin,Clave,'\n');

	Aux2 = Clave.substr(4,3);
	std::cout << Aux2 << std::endl; 
	Pos = (atoi(Aux2.c_str()) % 5000);
	std::cout << Pos << std::endl;
	std::cout << "Introduzca el Modelo del Auto: " << std::endl;
	std::getline(std::cin,Aux,'\n');

	Clave = Clave + Aux;

	std::cout << "Introduzca el Precio del Auto: " << std::endl;
	std::getline(std::cin,Aux3,'\n');

	Archivo_Hash.clear();
	Archivo_Hash.seekg((Pos*40),std::ios::beg);

	std::getline(Archivo_Hash,Aux,'\n');
	if(Aux[0] == '1')
	{
		Archivo_Hash.clear();
		Archivo_Hash.seekp((Pos*40),std::ios::beg);

		Archivo_Hash << "0 " << Clave << std::setw(13) << Aux3; // El setw escribe el precio 13 espacios adelante de la clave
	}

	Archivo_Hash.close();
	return 0;
}
