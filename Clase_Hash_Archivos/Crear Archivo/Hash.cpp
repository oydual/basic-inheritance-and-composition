#include <fstream>
#include <iostream>
#include <string>
#include <iomanip>

int main ()
{
	std::ofstream Escritura;
	Escritura.open("Hash.txt");
	if(!Escritura.good())
		return 2;

	for (int i = 0; i < 5000; ++i)
	{
		Escritura << '1' << std::setfill(' ') << std::setw(39) << "\n"; // Escritura de los Caracteres en Blanco
	}
	Escritura.close();
	return 0;
}