#include<iostream>
#include<fstream>
#include<string>
#include"CProducto.h"

using namespace std;


/*
* Archivo hash , con area de desborde.
* archivo hash principal -> Manejo de colisiones por area de desborde
*	archivo de desborde -> la insercion puede hacerse de dos maneras
*											-> por sondeo lineal, donde se encuentre la primera posicion vacia
*											-> en la misma clave que dio la primera funcion hash
*													y en caso de haber colision en el archivo de desborde se soluciona por sondeo lineal.
*	En este programa se hara la insersecion por sondo lineal.
*/
int main()
{
	CProducto pro;
	int length;
	fstream archivo;
	fstream auxiliar;
	fstream archivo_hash_p; //principal
	fstream archivo_hash_d; //desborder
	string linea;
	int ocupado;
	int prox;
	int clave;
	int ant;
	int ind = 0;
	int aux;
	bool vacio = false;

	//Apertura del archivo original
	archivo.open("productos.txt");

	//Creacion del archivo de tamanio fijo a partir del archivo original
	auxiliar.open("auxiliar.txt", fstream::out | fstream::app | fstream::in);

	//Obtenemos el tamanio del archivo
	auxiliar.seekg (0, auxiliar.end);
	length = auxiliar.tellg();
	auxiliar.seekg (0, auxiliar.beg);
	//Verificamos que el archivo este vacio para crearlo
	if(length == 0)
	{
		while(!archivo.good())
		{
			cerr << "ERROR: Al abrir archivo" << endl;
			return(1);
		}
		archivo >> pro;
		while(!archivo.eof())
		{

			if(!archivo.fail())
			{
				auxiliar << pro;
				archivo >> pro;
			}
		}
	}

	//Creamos el archivo hash principal
	//Asumiendo el mejor de los casos en que no existira ninguna colision
	//Creamos el archivo capaz de contener todos los registros.
	archivo_hash_p.open("hash_p.idx" , fstream::out | fstream::app | fstream::in);
	archivo_hash_p.seekg (0, archivo_hash_p.end);
	length = archivo_hash_p.tellg();
	archivo_hash_p.seekg (0, archivo_hash_p.beg);
	//Si el archivo no existe creamos las columnas vacias.
	if(length == 0)
	{
		for(int i=0 ; i < 2300 ; i++)
		{
	   		archivo_hash_p << std::left << std::setfill (' ') << std::setw(5) << i << ":";
	   		archivo_hash_p << std::right << std::setfill (' ') << std::setw(11) << ":";
	  		archivo_hash_p << std::right << std::setfill (' ') << std::setw(11)  << ":";
	   		archivo_hash_p << std::right << std::setfill (' ') << std::setw(11) << ":";
	   		archivo_hash_p << std::right << std::setfill (' ') << std::setw(61) << ":";
			  archivo_hash_p << std::right << std::setfill (' ') << std::setw(1) << 0 << ":";
				archivo_hash_p << std::right << std::setfill (' ') << std::setw(5) << -1 << ":" << endl;


	   }
	}
	archivo_hash_p.close();
	//Creamos el archivo de desborde.
	archivo_hash_d.open("hash_d.idx" , fstream::out | fstream::app | fstream::in);
	archivo_hash_d.seekg (0, archivo_hash_d.end);
	length = archivo_hash_d.tellg();
	archivo_hash_d.seekg (0, archivo_hash_d.beg);
	//Si el archivo no existe creamos las columnas vacias.
	//Asumimos el peor de los casos en el cual siempre se presente colision , por lo cual
	//El archivo de desborde debera  ser capaz tambien de almacenar todos los registros
	if(length == 0)
	{
		for(int i=0 ; i < 2300 ; i++)
		{
	   		archivo_hash_d << std::left << std::setfill (' ') << std::setw(5) << i << ":";
	   		archivo_hash_d << std::right << std::setfill (' ') << std::setw(11) << ":";
	  		archivo_hash_d << std::right << std::setfill (' ') << std::setw(11)  << ":";
	   		archivo_hash_d << std::right << std::setfill (' ') << std::setw(11) << ":";
	   		archivo_hash_d << std::right << std::setfill (' ') << std::setw(61) << ":";
				archivo_hash_d << std::right << std::setfill (' ') << std::setw(1) << 0 << ":";
				archivo_hash_d << std::right << std::setfill (' ') << std::setw(5) << -1 << ":" << endl;


	   }
	}
	archivo_hash_d.close();

	//Llenamos el archivo hash y su desborde
	archivo_hash_p.open("hash_p.idx" , fstream::out | fstream::in);
	archivo_hash_d.open("hash_d.idx" , fstream::out | fstream::in);

	auxiliar.seekg(ios_base::beg);
	while(!auxiliar.eof())
	{
		auxiliar >> pro;
		if(!auxiliar.fail())
		{
			//Calculamos la posicion en el archivo hash
			//Sin superar el tamanio del archivo
			clave = pro.ver_codigo() % 1517;
			archivo_hash_p.seekg(clave * 109);
			getline(archivo_hash_p,linea);
			ocupado = (int)atoi(linea.substr(100,1).c_str());
			prox = (int)atoi(linea.substr(102,5).c_str());
			//Si la posicion principal no esta ocupada procedemos a ocuparla con los datos del producto
			if(ocupado != 1)
			{
				archivo_hash_p.seekp(clave * 109);
				archivo_hash_p << std::left << std::setw(5) << clave << ":";
				archivo_hash_p << std::right << std::setw(10) << pro.ver_codigo() <<":";
				archivo_hash_p << std::right << std::setw(10)  << pro.ver_cantidad() <<":";
				archivo_hash_p << std::right << std::setw(10) << pro.ver_precio() <<":";
				archivo_hash_p << std::right << std::setw(60) << pro.ver_descripcion() <<":";
				archivo_hash_p << 1 << ":";
				archivo_hash_p << std::right << std::setfill (' ') << std::setw(5) << -1 << ":" << endl;
			}
			else
			{
				//Si proximo es igual a -1, buscamos por sondeo lineal desde el inicio del archivo de desborde
				//La primera posicion libre.
				if(prox == -1)
				{
					while(!vacio)
					{
						archivo_hash_d.seekg(ind * 109);
						getline(archivo_hash_d,linea);
						ant = clave;
						ocupado = (int)atoi(linea.substr(100,1).c_str());
						prox = (int)atoi(linea.substr(102,5).c_str());
						if(ocupado != 1)
						{
							archivo_hash_d.seekp(ind * 109);
							archivo_hash_d << std::left << std::setw(5) << ind << ":";
							archivo_hash_d << std::right << std::setw(10) << pro.ver_codigo() <<":";
							archivo_hash_d << std::right << std::setw(10)  << pro.ver_cantidad() <<":";
							archivo_hash_d << std::right << std::setw(10) << pro.ver_precio() <<":";
							archivo_hash_d << std::right << std::setw(60) << pro.ver_descripcion() <<":";
							archivo_hash_d << 1 << ":";
							archivo_hash_d << std::right << std::setfill(' ') << std::setw(5) << -1 << ":" << endl;
							archivo_hash_p.seekp((ant * 109)+102);
							archivo_hash_p << std::right << std::setfill(' ') << std::setw(5) << ind << ":" << endl;
							vacio = true;
						}
						ind++;
					}
					ind = 0;
					vacio = false;
				}
				else
				{
					//Quiere decir que ya a ocurrido una colision para esta clave por lo tanto procedemos a buscar la nueva posicion
					//A insertar en el archivo de desborde por sondeo lineal a partir de donde el registro indique que se encuentre el
					//Proximo, y al encontrar esta posicion guardar la anterior en el campo de proximo, para de esta manera reducir el
					//numero de registros a recorrer durante la busqueda.
					ind = prox;
					archivo_hash_d.seekg(ind * 109);
					getline(archivo_hash_d,linea);
					prox = (int)atoi(linea.substr(102,5).c_str());
					while(prox != -1)
					{
						ind = prox;
						archivo_hash_d.seekg(ind * 109);
						getline(archivo_hash_d,linea);
						prox = (int)atoi(linea.substr(102,5).c_str());
					}
					ant = ind;
					ind++;
					while(!vacio)
					{
						archivo_hash_d.seekg(ind * 109);
						getline(archivo_hash_d,linea);
						ocupado = (int)atoi(linea.substr(100,1).c_str());
						prox = (int)atoi(linea.substr(102,5).c_str());
						if(ocupado != 1)
						{
							archivo_hash_d.seekp(ind * 109);
							archivo_hash_d << std::left << std::setw(5) << ind << ":";
							archivo_hash_d << std::right << std::setw(10) << pro.ver_codigo() <<":";
							archivo_hash_d << std::right << std::setw(10)  << pro.ver_cantidad() <<":";
							archivo_hash_d << std::right << std::setw(10) << pro.ver_precio() <<":";
							archivo_hash_d << std::right << std::setw(60) << pro.ver_descripcion() <<":";
							archivo_hash_d << 1 << ":";
							archivo_hash_d << std::right << std::setfill(' ') << std::setw(5) << -1 << ":" << endl;
							archivo_hash_d.seekp((ant * 109)+102);
							archivo_hash_d << std::right << std::setfill(' ') << std::setw(5) << ind << ":" << endl;
							vacio = true;
						}
						ind++;
					}
					ind = 0;
					vacio = false;
				}
			}
		}
	}
	return(0);
}
