#include"CProducto.h"

CProducto::CProducto() {
   this->codigo = 0;
   this->cantidad = 0;
   this->precio = 0;
   this->descripcion = " ";
}

CProducto::CProducto(const CProducto& producto){
   this->codigo = producto.codigo;
   this->cantidad = producto.cantidad;
   this->precio = producto.precio;
   this->descripcion = producto.descripcion;
}

void CProducto::asignar(unsigned int codigo , unsigned int cantidad, float precio, std::string descripcion)
 {
 	this->codigo = codigo; 
 	this->cantidad = cantidad; 
 	this->precio = precio; 
 	this->descripcion = descripcion; 
 }
 
 void CProducto::asignar_codigo(unsigned int codigo)
 {
 	this->codigo = codigo; 
 }
 
 void CProducto::modificar_cantidad(unsigned int cantidad)
 {
 	this->cantidad = cantidad; 
 }

CProducto CProducto::operator =(const CProducto &producto)
{
	this->codigo = producto.codigo; 
	this->cantidad = producto.cantidad; 
	this->precio = producto.precio; 
	this->descripcion = producto.descripcion; 
	
	return(producto); 

}


int CProducto::operator ==(const CProducto &producto)
{
	if(this->codigo == producto.codigo)
		return(1); 
		
	return(0); 
}


std::ostream& operator <<(std::ostream& s, const CProducto& p){
   s << std::left << std::setw(10) <<  p.ver_codigo() << ":"; 
   s << std::left << std::setw(10) << p.ver_cantidad() << ":";
   s << std::left << std::setw(10) << p.ver_precio() << ":";
   s << std::right << std::setw(40) <<  p.ver_descripcion() << std::endl;
   
   return(s);
}


std::istream& operator >>(std::istream& e, CProducto &item){
   std::string cadenaAuxiliar; 
   unsigned int codigo, cantidad;
   float precio;
   std::string descripcion;
   
   std::getline(e, cadenaAuxiliar, ':');

   if (!e.fail()) {
      codigo = (unsigned int)atoi(cadenaAuxiliar.c_str());
   }

   std::getline(e, cadenaAuxiliar, ':');
	
   if (!e.fail()) {
      cantidad = (unsigned int)atoi(cadenaAuxiliar.c_str());
   }

   std::getline(e, cadenaAuxiliar, ':');

   if (!e.fail()) {
      precio = (float)atof(cadenaAuxiliar.c_str());
   }

   std::getline(e, cadenaAuxiliar);

   if (!e.fail()) {
      descripcion = cadenaAuxiliar;
   }

   item.asignar(codigo, cantidad, precio, descripcion);

   return(e);
}

