#include <iostream>


using namespace std;
class Base {
public:
virtual void foo() = 0;
virtual void bar() = 0;
};
class Der1 : public virtual Base {
public:
virtual void foo();
};
void Der1::foo()
{
        bar();
}
class Der2 : public virtual Base {
public:
virtual void bar();
};
void Der2::bar()
{
        cout << "Der2::bar()" << endl;
}
class Join : public Der1, public Der2 {};
int main()
{
        Der1* obj = new Join();
        obj->foo();
}
