#include <iostream>
#include <string>
#include <vector>

using namespace std;
enum nota { middleC, Csharp, Cflat };

class Instrumento {
public:
void normalTocar(nota a) const {
        switch (a)
        {
        case middleC:
                cout << "Instrumento::Tocando middleC" << endl;
                break;
        case Csharp:
                cout<< "Instrumento::Tocando Csharp" << endl;
                break;
        case Cflat:
                cout << "Instrumento::Tocando Cflat" << endl;
                break;
        default:
                break;
        }

}
virtual void virtualTocar(nota a) const {
        switch (a)
        {
        case middleC:
                cout << "Instrumento::Tocando middleC" << endl;
                break;
        case Csharp:
                cout<< "Instrumento::Tocando Csharp" << endl;
                break;
        case Cflat:
                cout << "Instrumento::Tocando Cflat" << endl;
                break;
        default:
                break;
        }
}
virtual void what() const {
        cout << "Instrumento" << endl;
}
};
class Viento : public Instrumento
{
public:
void normalTocar(nota a) const {
        switch (a)
        {
        case middleC:
                cout << "Viento::Tocando middleC" << endl;
                break;
        case Csharp:
                cout<< "Viento::Tocando Csharp" << endl;
                break;
        case Cflat:
                cout << "Viento::Tocando Cflat" << endl;
                break;
        default:
                break;
        }
}
void virtualTocar(nota a) const {
        switch (a)
        {
        case middleC:
                cout << "Viento::Tocando middleC" << endl;
                break;
        case Csharp:
                cout<< "Viento::Tocando Csharp" << endl;
                break;
        case Cflat:
                cout << "Viento::Tocando Cflat" << endl;
                break;
        default:
                break;
        }
}
void what(){
        cout << "Viento" << endl;
}
};
class Cuerda : public Instrumento
{
public:
void normalTocar(nota a) const {
        switch (a)
        {
        case middleC:
                cout << "Cuerda::Tocando middleC" << endl;
                break;
        case Csharp:
                cout<< "Cuerda::Tocando Csharp" << endl;
                break;
        case Cflat:
                cout << "Cuerda::Tocando Cflat" << endl;
                break;
        default:
                break;
        }
}
void virtualTocar(nota a) const {
        switch (a)
        {
        case middleC:
                cout << "Cuerda::Tocando middleC" << endl;
                break;
        case Csharp:
                cout<< "Cuerda::Tocando Csharp" << endl;
                break;
        case Cflat:
                cout << "Cuerda::Tocando Cflat" << endl;
                break;
        default:
                break;
        }
}
void what(){
        cout << "Cuerda" << endl;
}
};
class Percusion : public Instrumento
{
public:
void normalTocar(nota a) const {
        switch (a)
        {
        case middleC:
                cout << "Percusion::Tocando middleC" << endl;
                break;
        case Csharp:
                cout<< "Percusion::Tocando Csharp" << endl;
                break;
        case Cflat:
                cout << "Percusion::Tocando Cflat" << endl;
                break;
        default:
                break;
        }
}
void virtualTocar(nota a) const {
        switch (a)
        {
        case middleC:
                cout << "Percusion::Tocando middleC" << endl;
                break;
        case Csharp:
                cout<< "Percusion::Tocando Csharp" << endl;
                break;
        case Cflat:
                cout << "Percusion::Tocando Cflat" << endl;
                break;
        default:
                break;
        }
}
void what(){
        cout << "Cuerda" << endl;
}
};

void afinar(Instrumento& i) {
        i.normalTocar(middleC);
}

int main() {
        Viento flauta;
        Instrumento *cualquiera;
        Instrumento* armario[3];
        cualquiera = new Cuerda();
        armario[0] = new Viento();
        armario[1] = new Cuerda();
        armario[2] = new Percusion();
        afinar(flauta);
        armario[1]->virtualTocar(Csharp);
        cualquiera->virtualTocar(Csharp);
        Instrumento* cualquiera2 = cualquiera;
        cualquiera2->virtualTocar(Csharp);
        armario[2]->virtualTocar(Cflat);
}
