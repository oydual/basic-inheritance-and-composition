
using namespace std;

class Padre {
int _i;
protected:
int read() const {
        return i;
}
void set(int i) {
        _i = i;
}
public:
Padre(int i = 0) : _i(i) {
}
int value(int m) const {
        return m*_i;
}
};

class Hija : public Padre {
int _j;
public:
Hija(int j = 0) : _j(j) {
}
void change(int x) {
        set(x);
}
};

int main() {
        Hija d;
        d.change(10);
}
